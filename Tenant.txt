
Tenant configuration

Application start ---->>
					-->> Host (Start host, lookup domain name.)
					----------> Check for a DnsRecord
					---------->> DnsRecords
							-->> Record[1][1] = "mydomain.com"
							-->> Record[1][2] = "www.mydomain.com"
							---------->> Return a Tenant
									-->> Tenant = 1 // Load Tenant