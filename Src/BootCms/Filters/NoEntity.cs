﻿using FluentNHibernate.Automapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BootCms.Filters
{
    /// <summary>
    /// Tell the current Property to not be persisted to table.
    /// </summary>
    public class NoEntity : Attribute 
    { 
    }
}