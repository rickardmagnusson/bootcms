﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Mapping;
using BootCms.Cache;

namespace BootCms.Models
{
    public abstract class Module
    {
        public virtual Int32 Id { get; set; }

        //If the Module not allow placement, just add [NoEntity] attribute in your module.
        public virtual Region Region { get; set; }

        protected Module()
        {
        }
    }
}