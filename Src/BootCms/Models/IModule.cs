﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootCms.Models
{
    public interface IModule
    {
        int Id { get; set; }
        Region Region { get; set; }
    }
}
