﻿using BootCms.Data.DataManager;
using BootCms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootCms.Controllers
{
    /// <summary>
    /// Module Controller base class.
    /// </summary>
    /// <typeparam name="T">Module class</typeparam>
    public abstract class ModuleController<T> : Controller where T : class
    {
       
        public ModuleController() : base()
        {  
           
        }

        public ActionResult Index()
        {
            return View();
        }
	}
}