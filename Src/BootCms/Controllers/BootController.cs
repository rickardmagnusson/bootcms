﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootCms.Controllers
{
    public class BootController : BaseController
    {
        public BootController() : base()
        {
        }

        public ActionResult Index()
        {
            return View(Model);
        }
	}
}