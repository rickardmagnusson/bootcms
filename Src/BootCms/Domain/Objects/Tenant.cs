﻿using System;
using System.Collections.Generic;
using BootCms.Cache;

namespace BootCms.Domain.Objects
{
    public class Tenant : IEntity
    {
        public virtual Int32 Id { get; set; }
        public virtual Guid Key { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<DnsRecord> Records { get; set; } 
    }
}