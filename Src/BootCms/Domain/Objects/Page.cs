﻿using BootCms.Cache;
using BootCms.Filters;
using BootCms.Models;
using System;
using System.Collections.Generic;

namespace BootCms.Domain.Objects
{
    public class Page : IEntity
    {
        public virtual Int32 Id { get; set; }
        public virtual Int32 ParentId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Url { get; set; }      //Used for redirect
        public virtual string MetaTitle { get; set; }
        public virtual string Controller { get; set; }
        public virtual string Action { get; set; }
        public virtual bool Active { get; set; }

        [NoEntity]
        public virtual List<Module> Modules{ get; set; }
    }
}