﻿using System;
using System.Web;
using BootCms.Cache;
using BootCms.Extensions;
using BootCms.Filters;
using NHibernate.Linq;

namespace BootCms.Domain.Objects
{
    public class DnsRecord : IEntity
    {
        public virtual Int32 Id { get; set; }
        public virtual Int32 TenantId { get; set; } // Tenant id
        public virtual string Record { get; set; }  // Multiple dns records for e.g www.mydomain.com or mydomain.com maps to same domain.

        /// <summary>
        /// Get current DnsRecord requested by Hostname
        /// </summary>
        /// <returns>Tenant</returns>
        public virtual DnsRecord Resolve()
        {
            var records = BootStarter.SessionFactory.OpenSession();
            return records.QueryOver<DnsRecord>().Where(d => d.Record == AppDataFolder.Domain()).SingleOrDefault();
        }
    }
}