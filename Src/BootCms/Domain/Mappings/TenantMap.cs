﻿using BootCms.Data;
using BootCms.Domain.Objects;
using System;

namespace BootCms.Domain.Mappings
{
    public class TenantMap : Entity<Tenant>
    {
        public TenantMap()
        {
            Id(x => x.Id)
             .Column("Id")
             .GeneratedBy.Assigned()
             .CustomType<Int32>();
            Map(p=> p.Key);
            Map(p => p.Name);
        }
    }
}