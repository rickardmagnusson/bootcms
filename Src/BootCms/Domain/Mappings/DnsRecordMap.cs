﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using BootCms.Data;
using BootCms.Domain.Objects;
using Microsoft.Ajax.Utilities;

namespace BootCms.Domain.Mappings
{
    public class DnsRecordMap : Entity<DnsRecord>
    {
        public DnsRecordMap()
        {
            Id(x => x.TenantId)
             .Column("Id")
             .CustomType<Int32>();
            Map(x => x.TenantId);
            Map(x => x.Record);
        }
    }
}