﻿using System;
using BootCms.Data;
using BootCms.Domain.Objects;

namespace BootCms.Domain.Mappings
{
    public class PageMap : Entity<Page>
    {
        public PageMap()
        {
            Id(x => x.Id)
             .Column("Id")
             .GeneratedBy.Assigned()
             .CustomType<Int32>();
            Map(p => p.Title);
            Map(p => p.Url);
            Map(p => p.ParentId);
            Map(p => p.Active);
            Map(p => p.MetaTitle);
            Map(p => p.Controller);
            Map(p => p.Action);
        }
    }
}