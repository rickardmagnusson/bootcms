﻿using System.Web;
using Autofac.Extras.Multitenant;
using BootCms.Domain.Objects;

namespace BootCms.Environment
{
    public class PerDomainStrategy : ITenantIdentificationStrategy
    {
        public bool TryIdentifyTenant(out object tenantId)
        {
            tenantId = null;
            try
            {
                var context = HttpContext.Current;
                if (context != null)
                {
                    var domain = context.Request.Url.Authority;
                    var dns = BootStarter.SessionFactory.OpenSession().QueryOver<DnsRecord>().Where(t=> t.Record==domain).SingleOrDefault();
                    tenantId = BootStarter.SessionFactory.OpenSession().QueryOver<Tenant>().Where(t => t.Id == dns.TenantId);
                }
            }
            catch (HttpException)
            {
                //Occurs at app startup in IIS 7.0
            }
            return tenantId != null;
        }
    }
}