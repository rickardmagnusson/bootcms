﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootCms.Environment
{
    public interface IPerDomainStrategy
    {
        bool TryIdentifyTenant(out object id);
    }
}
