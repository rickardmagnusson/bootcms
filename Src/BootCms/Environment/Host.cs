﻿using System;
using System.Linq;
using Autofac;
using Autofac.Core;
using Autofac.Extras.Multitenant;
using BootCms.Domain.Objects;
using BootCms.Models;
using NHibernate;

namespace BootCms.Environment
{
    /// <summary>
    /// Host initializer
    /// There is only one Host in this application.
    /// Host serves application with Tenants.
    /// </summary>
    public sealed class Host
    {
        private static volatile Host _instance;
        private static readonly object SyncRoot = new Object();
        public ISession Session { get; private set; }


        public static Host CreateHost(Action<ContainerBuilder> registrations)
        {
            var container = CreateHostContainer(registrations);
            return container.Resolve<Host>();
        }

        private Host()
        {
            
        }

        public static IContainer CreateHostContainer(Action<ContainerBuilder> registrations)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Tenant>().As<ITenant>().InstancePerTenant();
            builder.Register(c =>
            {
                var s = c.Resolve<ITenantIdentificationStrategy>();
                object id;
                s.TryIdentifyTenant(out id);
                return id;
            })
            .Keyed<object>("tenantId");
            builder.RegisterType<Tenant>()
                   .As<ITenant>()
                   .WithParameter(
                     (pi, c) => pi.Name == "tenantId",
                     (pi, c) => c.ResolveKeyed<object>("tenantId")).InstancePerDependency();

            

            registrations(builder);

            var container = builder.Build();

            var mtc = new MultitenantContainer(new PerDomainStrategy(), container);


            //Register ViewEngines, ControllerBuilder etc here.

            return container;
        }

        public static Host CreateInstance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null) { 
                            _instance = new Host();
                        }
                    }
                }
                return _instance;
            }
        }

        /*
        private static void InitTenant()
        {
            _instance.Tenant = BootStarter.SessionFactory.OpenSession()
                .Get<Tenant>(new DnsRecord()
                .Resolve().Id);
        }*/
    }
}