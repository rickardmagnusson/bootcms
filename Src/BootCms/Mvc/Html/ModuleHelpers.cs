﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootCms.Mvc.Html
{
    public static partial class HtmlHelpers
    {
        public static HtmlString Zone(this HtmlHelper html, Region region)
        {
            return new HtmlString(region.ToString());
        }
    }
}