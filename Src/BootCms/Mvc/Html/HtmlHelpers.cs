﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BootCms.Mvc.Html
{
    public static partial class HtmlHelpers
    {
        /// <summary>
        /// Translate a string from resources
        /// </summary>
        /// <param name="html">HtmlHelper</param>
        /// <param name="val">The string to translate</param>
        /// <returns>A htmlstring containg the translated value</returns>
        public static string R(this HtmlHelper html, string toTranslate)
        {
            var val = from resources in ToDictionary(ResourceList("R"))
                    select resources
                        into resource
                        where resource.Key.ToString().ToLower() == toTranslate.ToLower()
                        select resource;

            return (val==null) ? toTranslate : val.ToString();
        }

        /// <summary>
        /// Create a Label with Resource text.
        /// </summary>
        /// <param name="html">HtmlHelper</param>
        /// <param name="toTranslate">The key to get the value from</param>
        /// <param name="htmlAttributes">Array of attibutes for the label</param>
        /// <returns>A label with value attached</returns>
        public static IHtmlString ResourceLabel(this HtmlHelper html, string toTranslate, object htmlAttributes)
        {
            var builder = new TagBuilder("label");
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            builder.InnerHtml = html.R(toTranslate);

            return new HtmlString(builder.ToString());
        }

        //ResourceFile for e.g "R" or "R.se".
        private static ResXResourceReader ResourceList(string resourceFile)
        {
            return new ResXResourceReader(Path.Combine(HttpRuntime.AppDomainAppPath, string.Format("App_GlobalResources/{0}.resx", resourceFile)));
        }

        //Cast the Resource entries to IEnumerable<DictionaryEntry>.
        private static IEnumerable<DictionaryEntry> ToDictionary(ResXResourceReader reader)
        {
            return reader.Cast<DictionaryEntry>();
        }
    }
}