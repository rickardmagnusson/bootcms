﻿using System;
using System.Configuration;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using BootCms.Cache;
using BootCms.Extensions;
using BootCms.Filters;
using BootCms.Filters.Utils;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Conf = NHibernate.Cfg.Configuration;

namespace BootCms
{
    public static class BootStarter
    {
        private static ISessionFactory _sessionFactory;

        /// <summary>
        ///     Get current SessionFactory.
        /// </summary>
        public static ISessionFactory SessionFactory
        {
            get { return _sessionFactory ?? (_sessionFactory = CreateConfig().BuildSessionFactory()); }
            set { _sessionFactory = value; }
        }

        /// <summary>
        ///     Init nHibernate config and start Session.
        /// </summary>
        public static void Init()
        {
            var container = CreateConfig();
            _sessionFactory = container.BuildSessionFactory();
        }

        /// <summary>
        ///     Create nHibernate configuration.
        /// </summary>
        /// <returns>nHibernate Configuration</returns>
        private static Conf CreateConfig()
        {
            return Fluently.Configure()
                .Database(DatabaseConfig)
                .Mappings(MapAssemblies)
                .ExposeConfiguration(ValidateSchema)
                .ExposeConfiguration(BuildSchema)
                .BuildConfiguration();
        }

        /// <summary>
        ///     Maps all Entities in bin folder.
        /// </summary>
        /// <param name="fmc">MappingConfiguration mapping</param>
        private static void MapAssemblies(MappingConfiguration fmc)
        {
            (from a in AppDomain.CurrentDomain.GetAssemblies()
                select a
                into assemblies
                select assemblies)
                .ToList()
                .ForEach(a =>
                {   //Ignore other assemblies since there's no Entity's created in them. (MSCoreLib makes load fail)
                    if (a.FullName.StartsWith("BootCms")) { 
                        fmc.AutoMappings.Add(AutoMap.Assembly(a)
                            .OverrideAll(p => p.SkipProperty(typeof (NoEntity)))
                            .Where(IsEntity));
                    }
                });
        }

        /// <summary>
        ///     Check when either a Type is an Entity.
        /// </summary>
        /// <param name="t">Type to check</param>
        /// <returns>True if Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof (IEntity).IsAssignableFrom(t);
        }

        /// <summary>
        ///     Set database information
        /// </summary>
        /// <returns>IPersistenceConfigurer configuration</returns>
        private static IPersistenceConfigurer DatabaseConfig()
        {
            switch (ConfigurationManager.AppSettings["DB"])
            {
                case "BootCmsSql2008":
                    return MsSqlConfiguration.MsSql2008
                        .UseOuterJoin()
                        .ConnectionString(x => x.FromConnectionStringWithKey("BootCmsSql2008"))
                        .ShowSql();

                case "BootCmsSqlCe40": //Only this works as multiTenant database at the time.
                    return MsSqlCeConfiguration.MsSqlCe40
                        .UseOuterJoin()
                        .ConnectionString(new SqlCeEngine().CreateConnectionString("BootCms"))
                        .ShowSql();

                case "BootCmsMySql5":
                    return MySQLConfiguration.Standard
                        .UseOuterJoin()
                        .ConnectionString(x => x.FromConnectionStringWithKey("BootCmsMySql5"))
                        .ShowSql();
            }

            throw new ArgumentException(@"Invalid DB. Appsettings key has fault setting. 
                                            Use for e.g BootCmsSql2008, BootCmsSqlCe40 or BootCmsMySql5");
        }

        /// <summary>
        ///     Validates nHibernate schema
        /// </summary>
        /// <param name="config">Configuration config</param>
        private static void ValidateSchema(Conf config)
        {
            var check = new SchemaValidator(config);
        }

        /// <summary>
        ///     Creates or build the current database.
        /// </summary>
        /// <param name="config"></param>
        private static void BuildSchema(Conf config)
        {
            SchemaMetadataUpdater.QuoteTableAndColumns(config);
            new SchemaUpdate(config).Execute(false, true);
        }
    }
}