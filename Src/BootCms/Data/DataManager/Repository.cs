﻿using BootCms.Data.Interfaces;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace BootCms.Data.DataManager
{
    /// <summary>
    /// Repository. Contains dynamic operations to serve Models.
    /// </summary>
    /// <typeparam name="T">Model</typeparam>
    public class Repository : IRepository
    {
        private static ISession data;

        public Repository(ISession session)
        {
            data = session;
        }

        /// <summary>
        /// Get the specified type
        /// </summary>
        /// <typeparam name="T">The type to get</typeparam>
        /// <param name="id">Int32 Id to get</param>
        /// <returns></returns>
        public T Get<T>(Int32 id)
        {
            return data.Get<T>(id);
        }


        /// <summary>
        /// Returns a List T
        /// </summary>
        /// <typeparam name="T">Type to get</typeparam>
        /// <returns>A list of the specified type.</returns>
        public IList<T> List<T>() where T : class
        {
            return data.QueryOver<T>().List<T>();
        }


        public void Save(dynamic item)
        {
            if (data.Transaction.IsActive) { 
                data.Save(item);
                data.Flush();
            }
        }
    }
}