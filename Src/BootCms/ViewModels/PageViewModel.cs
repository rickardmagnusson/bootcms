﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using BootCms.Models;
using BootCms.Domain.Objects;
using BootCms.Data.DataManager;

namespace BootCms.ViewModels
{
    public class PageViewModel
    {
        Repository _Repository{ get; set;}

        public PageViewModel(ISession session)
        {
            _Repository = new Repository(session);
        }

        public Page Page
        {
            get {

                try { 
                    return new Page().CurrentPage();
                }
                catch //If no page in database, just create one in memory. 
                {
                    var page = new Page { Id = 1, Title = "Start", Controller = "Boot", Action = "Index", Active = true, ParentId = 0, MetaTitle = "Home page", Url = "/" };
                    _Repository.Save(page);
                    return page;
                }
            }
        }

        public IList<Module> Modules
        {
            get { return _Repository.List<Module>(); }
        }
    }
}