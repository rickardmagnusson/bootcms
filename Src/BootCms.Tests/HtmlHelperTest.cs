﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BootCms;
using BootCms.Mvc.Html;
using BootCms.Controllers;
using System.Web.Mvc;
using System.Collections;
using System.Web;
using System.Web.Helpers;
using System.IO;

namespace BootCms.Tests
{
    [TestClass]
    public class HtmlHelperTest
    {
        string path = string.Empty;

        [TestMethod]
        public void Test_MapPath()
        {
            var context = new FakeHttpContext();
            string pathToFile = context.Request.MapPath("App_GlobalResources/R.resx");
            if (System.IO.File.Exists(pathToFile))
                path = pathToFile;
            else
                throw new FileNotFoundException();
        }

        [TestMethod]
        public void Should_Return_A_String_With_ApplicationName_From_ResourceFile()
        {
            var vc = new ViewContext();
            vc.HttpContext = new FakeHttpContext();
            var html = new HtmlHelper(vc, new FakeViewDataContainer());

            var context = new FakeHttpContext();
            string pathToFile = context.Request.MapPath("App_GlobalResources\\R.resx");

            var resource = html.R("ApplicationName");
            Console.WriteLine(resource);
        }

        internal class FakeHttpContext : HttpContextBase
        {
            public override HttpRequestBase Request { get { return new FakeHttpRequest(); } }
        }

        internal class FakeHttpRequest : HttpRequestBase
        {
            public override string MapPath(string virtualPath)
            {
                return System.IO.Path.Combine(@"D:\Projects\2014\Git\Bitbucket\BootCMS\BootCMS\Src\BootCms\", virtualPath);
            }
        }

        internal class FakeViewDataContainer : IViewDataContainer
        {
            private ViewDataDictionary _viewData = new ViewDataDictionary();
            public ViewDataDictionary ViewData { get { return _viewData; } set { _viewData = value; } }
        }
    }
}
